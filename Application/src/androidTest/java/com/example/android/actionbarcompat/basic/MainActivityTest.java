package com.example.android.actionbarcompat.basic;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

@LargeTest
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> rule  = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void identityShouldReturnInputValue() {
        assertEquals(42, rule.getActivity().id(42));
    }

    @Test
    public void youShouldFixMe() {
        fail(":-(");
    }
}