package com.example.android.actionbarcompat.basic.examples;

public class BankAccount
{
    private String customerName;

    private double balance;

    private boolean frozen = false;

    private BankAccount()
    {
    }

    public BankAccount(String customerName, double balance)
    {
        this.customerName = customerName;
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public double getBalance() {
        return balance;
    }

    public void debit(double amount)
    {
        if (frozen)
        {
            throw new RuntimeException("Konto eingefroren.");
        }

        if (amount > balance)
        {
            throw new RuntimeException("Kontoüberzug");
        }

        if (amount < 0)
        {
            throw new IllegalArgumentException("amount muss >= 0 sein");
        }

        balance += amount;
    }

    public void credit(double amount)
    {
        if (frozen)
        {
            throw new RuntimeException("Konto eingefroren.");
        }

        if (amount < 0)
        {
            throw new IllegalArgumentException("amount muss >= 0 sein");
        }

        balance += amount;
    }

    public void freezeAccount() {
        frozen = true;
    }

    public void unfreezeAccount() {
        frozen = false;
    }

}
